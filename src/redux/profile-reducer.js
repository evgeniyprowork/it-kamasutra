import {profileAPI, usersAPI} from "../api/api";

const ADD_POST = "ADD-POST"
const SET_USER_PROFILE = "SET_USER_PROFILE"
const SET_STATUS = "SET_STATUS"

const initialState = {
	posts: [
		{id: 1, message: 'Nice to meet you!', likeCount: 5},
		{id: 2, message: 'This is my first post', likeCount: 47},
		{id: 3, message: 'This is .......', likeCount: 22},
		{id: 4, message: 'My first post', likeCount: 34},
	],
	profile: null,
	status: ""
}

const profileReducer = (state = initialState, action) => {
	switch (action.type) {
		case ADD_POST: {
			const newPost = {
				id: 5,
				message: action.newPostText,
				likeCount: 0
			}
			return {
				...state,
				posts: [...state.posts, newPost], // вместо PUSH что ниже добавляем newPost, тоже самое
				newPostText: '', // зануление
				//stateCopy.posts.push(newPost)
			}
		}
		case SET_STATUS: {
			return {
				...state,
				status: action.status,
			}
		}
		case SET_USER_PROFILE: {
			return {
				...state,
				profile: action.profile
			}
		}
		default:
			return state
	}
}

export const addPostActionCreator = (newPostText) => ({type: ADD_POST, newPostText})
export const setUserProfile = (profile) => ({type: SET_USER_PROFILE, profile})
export const setStatus = (status) => ({type: SET_STATUS, status})

export const getUserProfile = (userID) => (dispatch) => {
	usersAPI.getProfile(userID)
		.then(response => {
			dispatch(setUserProfile(response.data))
		})
}
export const getStatus = (userID) => (dispatch) => {
	profileAPI.getStatus(userID)
		.then(response => {
			// debugger
			dispatch(setStatus(response.data))
		})
}
export const updateStatus = (status) => (dispatch) => {
	profileAPI.updateStatus(status)
		.then(response => {
			if (response.data.resultCode === 0) {
				dispatch(setStatus(status))
			}
		})
}


export default profileReducer
