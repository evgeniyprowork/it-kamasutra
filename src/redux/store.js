import profileReducer from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";
import sidebarReducer from "./sidebar-reducer";

const store = {
	_state: {
		profilePage: {
			posts: [
				{id: 1, message: 'Nice to meet you!', likeCount: 5},
				{id: 2, message: 'This is my first post', likeCount: 47},
				{id: 3, message: 'This is .......', likeCount: 22},
				{id: 4, message: 'My first post', likeCount: 34},
			],
			newPostText: 'Hello Geka',
		},
		dialogsPage: {
			dialogs: [
				{id: 1, name: 'Evgeniy'},
				{id: 2, name: 'Jack'},
				{id: 3, name: 'Nick'},
				{id: 4, name: 'Sam'},
				{id: 5, name: 'Roger'},
			],
			messages: [
				{id: 1, message: 'Hi'},
				{id: 2, message: 'How are you?'},
				{id: 3, message: 'When u back?'},
				{id: 4, message: 'Bye'},
				{id: 5, message: 'LOL'},
			],
			newMessageBody: ""
		},
		sidebar: {}
	},
	_callSubscriber() {
		console.log('State .......');
	},

	getState() {
		return this._state
	},
	subscribe(observer) {
		this._callSubscriber = observer // observer - это по сути паттерн
	},

	dispatch(action) {
		this._state.profilePage = profileReducer(this._state.profilePage, action)
		this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action)
		this._state.sidebar = sidebarReducer(this._state.sidebar, action)

		this._callSubscriber(this._state);
	}
}

export default store