const SEND_MESSAGE = "SEND_MESSAGE"

const initialState = {
	dialogs: [
		{id: 1, name: 'Evgeniy'},
		{id: 2, name: 'Jack'},
		{id: 3, name: 'Nick'},
		{id: 4, name: 'Sam'},
		{id: 5, name: 'Roger'},
	],
	messages: [
		{id: 1, message: 'Hi'},
		{id: 2, message: 'How are you?'},
		{id: 3, message: 'When u back?'},
		{id: 4, message: 'Bye'},
		{id: 5, message: 'LOL'},
	]
}

const dialogsReducer = (state = initialState, action) => {
	switch (action.type) {
		case SEND_MESSAGE: {
			let body = action.newMessageBody
			return {
				...state, // создаем копию и работаем только через КОПИЮ
				messages: [...state.messages, {id: 6, message: body}], // вместо push что ниже
			}
			// stateCopy.messages.push({id: 6, message: body}), не используется, юзать как выше
		}

		default:
			return state
	}
}

export const sendMessageCreator = (newMessageBody) => {
	return {
		type: SEND_MESSAGE,
		newMessageBody
	}
}

export default dialogsReducer;