import React from 'react';
import s from './Dialogs.module.css'
import DialogItem from "./DialogItem/DialogItem";
import Message from "./Message/Message";
import {Redirect} from "react-router-dom";
import {Field, reduxForm} from "redux-form";
import {Textarea} from "../common/FormsControl/FormsControl";
import {maxLength, required} from "../../utils/validators/validators";

const Dialogs = props => {
	let state = props.dialogsPage

	const dialogsElements = state.dialogs.map(item => <DialogItem name={item.name} id={item.id}/>)
	const messagesElements = state.messages.map(item => <Message message={item.message}/>)
	const newMessageBody = state.newMessageBody

	const addNewMessage = (value) => {
		props.sendMessage(value.newMessageBody);
	};

	// if (!props.isAuth) return <Redirect to='/login'/>

	return (
		<section className={s.dialogs}>
			<div className={s.dialogsItems}>
				{dialogsElements}
			</div>
			<div className={s.messages}>
				<div>{messagesElements}</div>
				<AddMessageFormRedux onSubmit={addNewMessage}/>
			</div>
		</section>
	);
};

const maxLength50 = maxLength(50)

const AddMessageForm = (props) => {
	return (
		<form onSubmit={props.handleSubmit}>
			<div>
				<Field component={Textarea}
							 validate={[required, maxLength50]}
							 name="newMessageBody"
							 placeholder="Enter your message"/>
			</div>
			<div>
				<button>Send</button>
			</div>
		</form>
	)
}

const AddMessageFormRedux = reduxForm({
	form: 'dialogAddMessageForm'
})(AddMessageForm)

export default Dialogs;