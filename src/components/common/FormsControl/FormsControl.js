import React from 'react';
import styles from './FormsControl.module.css'

const FormsControl = ({input, meta, ...props})=> {
	const hasError = meta.touched && meta.error

	return (
		<div className={styles.formsControl + ' ' + (hasError ? styles.error : '')}>
			<div>
				{React.createElement(props.el, {...input, ...props})}
			</div>
			{hasError && <span>{meta.error}</span>}
		</div>
	)
}

export const Textarea = (props) => <FormsControl el="textarea" {...props}/>
export const Input = (props) => <FormsControl el="input" {...props}/>

/*
export const Textarea = ({input, meta, ...props}) => {
	const hasError = meta.touched && meta.error

	return (
		<div className={styles.formsControl + ' ' + (hasError ? styles.error : '')}>
			<div>
				<textarea {...input} {...props}/>
			</div>
			{hasError && <span>{meta.error}</span>}
		</div>
	)
}

export const Input = ({input, meta, ...props}) => {
	const hasError = meta.touched && meta.error

	return (
		<div className={styles.formsControl + ' ' + (hasError ? styles.error : '')}>
			<div>
				<input {...input} {...props}/>
			</div>
			{hasError && <span>{meta.error}</span>}
		</div>
	)
}*/
