import React from 'react';
import s from './NavBar.module.css';
import {NavLink} from "react-router-dom";

const NavBar = () => (
	<nav className={s.nav}>
		{/*<NavLink className={`${s.item} ${s.active}`} to="/profile" activeClassName={s.active}>Profile</NavLink>*/}
		<NavLink to="/profile" activeClassName={s.active}>Profile</NavLink>
		<NavLink to="/dialogs" activeClassName={s.active}>Messages(Dialogs)</NavLink>
		<NavLink to="/news">News</NavLink>
		<NavLink to="/music">Music</NavLink>
		<NavLink to="/settings">Settings</NavLink>
		<NavLink to="/users">Users</NavLink>
	</nav>
);

export default NavBar;