import React from 'react';
import s from './ProfileInfo.module.css';
import Preloader from "../../common/Preloader";
import ProfileStatus from "./ProfileStatus";

const ProfileInfo = (props) => {
	// когда высветило photos of null, это связано с тем что компонент в начале отрендерился а затем только промис что то вернул, т.е. во время рендера данных еще нет, прелоадер решает эту проблему
	if (!props.profile) {
		return <Preloader/>
	}

	return (
		<div>
			{/*<div className={s.profileInfo_img_w}>
				<img src="https://i.pinimg.com/originals/37/55/bd/3755bd0d6637277391a32f3b84faeeac.jpg" alt="beach"/>
			</div>*/}

			<section className={s.descriptionBlock}>
				<img src={props.profile.photos.large} alt="userPhoto"/>
				<ProfileStatus status={props.status} updateStatus={props.updateStatus}/>
				{/*<p>{props.profile.fullName}</p>
				<p>{props.profile.aboutMe}</p>*/}
			</section>
		</div>
	);
};

export default ProfileInfo;