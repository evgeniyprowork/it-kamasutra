import React from 'react';
import s from './MyPosts.module.css'
import Post from "./Post/Post";
import {Field, reduxForm} from "redux-form";
import {maxLength, required} from "../../../utils/validators/validators";
import {Textarea} from "../../common/FormsControl/FormsControl";

const maxLength15 = maxLength(15)

const MyPosts = (props) => {
	const postsElements = props.posts.map(item =>
		<Post
			message={item.message}
			likeCount={item.likeCount}/>)

	const onAddPost = (value) => {
		props.addPost(value.newPostText);
	};

	let AddNewPostForm = (props) => {
		return (
			<form onSubmit={props.handleSubmit}>
				<div>
					<Field
						component={Textarea}
						name="newPostText"
						validate={[required, maxLength15]}
						placeholder="Type any post"/>
				</div>
				<div>
					<button>Add post</button>
				</div>
			</form>
		)
	}

	AddNewPostForm = reduxForm({
		form: 'ProfileAddNewPostForm'
	})(AddNewPostForm)

	return (
		<section className={s.postsBlock}>
			<h3>My posts</h3>
			{/*<div>
				<div>
					<textarea
						onChange={onPostChange}
						ref={newPostElement}
						value={props.newPostText}/>
				</div>
				<div>
					<button onClick={onAddPost}>Add post</button>
				</div>
			</div>*/}
			<AddNewPostForm onSubmit={onAddPost} />
			<div className={s.posts}>
				{postsElements}
			</div>
		</section>
	);
};

export default MyPosts;