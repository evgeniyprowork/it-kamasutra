import React from 'react';
import s from './Post.module.css'

const Post = (props) => {
	return (
		<div className={s.item}>
			<img
				src="https://koolinus.files.wordpress.com/2019/03/avataaars-e28093-koolinus-1-12mar2019.png"
				alt=""/>
			{props.message}
			<p>Like: {props.likeCount}</p>
		</div>
	);
};

export default Post;