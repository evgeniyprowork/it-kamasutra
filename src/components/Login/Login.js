import React from 'react';
import {Field, reduxForm} from "redux-form";
import {Input} from "../common/FormsControl/FormsControl";
import {required} from "../../utils/validators/validators";
import {connect} from "react-redux";
import {login} from "../../redux/auth-reducer";
import {Redirect} from "react-router-dom";
import style from "../common/FormsControl/FormsControl.module.css"

const LoginForm = props => (
	<form onSubmit={props.handleSubmit}>
		<div><Field component={Input} placeholder={"Email"} name={"email"} validate={[required]}/></div>
		<div><Field component={Input} placeholder={"Password"} name={"password"} type={"password"} validate={[required]}/>
		</div>
		<div><Field component={Input} type={"checkbox"} name={"rememberMe"}/> Remember me</div>

		{props.error && <div className={style.formSummaryError}>
			{props.error}
		</div>}

		<button>Login</button>
	</form>
);

const LoginReduxForm = reduxForm({
	form: 'login'
})(LoginForm)

function Login(props) {
	const onSubmit = (formData) => {
		props.login(formData.email, formData.password, formData.rememberMe)
	};

	if (props.isAuth) {
		return <Redirect to={"/profile"}/>
	}

	return <div>
		<h1>LOGIN</h1>
		<LoginReduxForm onSubmit={onSubmit}/>
	</div>;
}

const mapStateToProps = state => ({
	isAuth: state.auth.isAuth
})

export default connect(mapStateToProps, {login})(Login);